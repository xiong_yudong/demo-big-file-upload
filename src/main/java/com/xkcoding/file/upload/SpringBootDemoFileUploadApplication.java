package com.xkcoding.file.upload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * 启动器
 * </p>
 *
 * @author yangkai.shen
 * @date Created in 2018-12-12 13:59
 */
@SpringBootApplication
public class SpringBootDemoFileUploadApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDemoFileUploadApplication.class, args);
    }
}
